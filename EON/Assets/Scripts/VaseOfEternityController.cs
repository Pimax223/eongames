﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseOfEternityController : ActivatingObject
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Sword")
        {
            TryToActivate();
        }
    }
}
