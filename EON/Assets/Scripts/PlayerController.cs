﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerController : MonoBehaviour {


    public Animator playerAnim;
    public MeshCollider swordCollider;
    private float vertAxis;
    private float horizAxis;
    private float mouseInputPosX;
    
	// Use this for initialization
	void Start () {

        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame

    private void FixedUpdate()
    {
        CharacterMotion();
        CameraHorizRotation();
        AttackCheck();
        CheckJump();
    }

    private void CharacterMotion()
    {
        horizAxis = Input.GetAxis("Horizontal");
        vertAxis = Input.GetAxis("Vertical");
        playerAnim.SetFloat("Vertical", vertAxis);
        playerAnim.SetFloat("Horizontal", horizAxis);
    }
    private void CameraHorizRotation()
    {
        mouseInputPosX = Input.GetAxis("Mouse X");
        transform.rotation *= Quaternion.Euler(0, mouseInputPosX, 0);

    }
    private void AttackCheck()
    {
        if (Input.GetMouseButtonDown(0))
        {
                playerAnim.ResetTrigger("RightClickAttack");
                Debug.Log("LeftAttack");
                playerAnim.SetTrigger("LeftClickAttack"+Random.Range(1,3));
            
        }
        if (Input.GetMouseButtonDown(1))
        {
                Debug.Log("RightAttack");
                playerAnim.SetTrigger("RightClickAttack");
        }
    }
    private void CheckJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerAnim.SetTrigger("Jump");
        }
    }
    public void EnableSwordCollider()
    {
        swordCollider.enabled = true;
    }
    public void DisableSwordCollider()
    {
        swordCollider.enabled = false;
    }
}
