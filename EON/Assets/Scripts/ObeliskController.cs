﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObeliskController : ActivatingObject
{
    
    public Transform controllPoint;
    public float currAngle;
    public float finalAngle = 0;
    private Coroutine angleChecker;



    IEnumerator AngleChecker()
    {
        Vector3 prev = GameController.instance.PlayerContr.transform.position - transform.position;
        Vector3 curr;
        while (true)
        {
            curr = GameController.instance.PlayerContr.transform.position - transform.position;
            currAngle = Vector3.SignedAngle(curr, prev, Vector3.up);
            finalAngle += currAngle;
            
            if (finalAngle > 360||finalAngle<-360)
            {
                TryToActivate();
                StopCoroutine(angleChecker);
            }
            prev = curr;
            yield return new WaitForFixedUpdate();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerCharacter")
        {
            angleChecker = StartCoroutine(AngleChecker());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "PlayerCharacter")
        {
            StopCoroutine(angleChecker);
            finalAngle = 0;
        }
    }

}
