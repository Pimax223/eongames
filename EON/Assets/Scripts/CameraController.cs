﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform CamPos;
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, CamPos.position, 0.15f);
        transform.rotation = Quaternion.Lerp(transform.rotation, CamPos.rotation, 0.15f);
	}
}
