﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    public static GameController instance;
    public float gameRadius;
    public float minDistanseBetweenObjects;
    public PlayerController PlayerContr;
    public ActivatingObject[] objectsToActivate;
    [SerializeField]
    private int activationCount;
    public int ActivationCount
    {
        get { return activationCount; }
        set
        {
            activationCount = value;
            if (activationCount == objectsToActivate.Length)
            {
                Debug.Log("You win!");
                Invoke("ReloadScene", 3);
            }
        }
    }



    // Use this for initialization
    void Start()
    {

        if (instance == null) instance = this;
        PlayerContr = GameObject.Find("PlayerCharacter").GetComponent<PlayerController>();
        foreach (ActivatingObject o in objectsToActivate)
        {
            o.InitializeObject();
        }
        LocateObjects(objectsToActivate.Length + 1);
        ChangeObjectsSequense(objectsToActivate);

    }

    private void LocateObjects(int count)
    {
        Vector3[] objPositions = new Vector3[count];
        bool isGood;
        do
        {
            isGood = true;
            for (int i = 0; i < count; i++)
            {
                objPositions[i] = new Vector3(Random.Range(-gameRadius, gameRadius), 0, Random.Range(-gameRadius, gameRadius));
            }
            for (int i = 0; i < count - 1; i++)
            {
                for (int j = i + 1; j < count; j++)
                {
                    if (Vector3.Distance(objPositions[i], objPositions[j]) < minDistanseBetweenObjects) isGood = false;
                }
            }
        } while (!isGood);

        for (int i = 0; i < count - 1; i++)
        {
            objectsToActivate[i].transform.position = objPositions[i];
        }
        PlayerContr.transform.position = objPositions[count - 1];
    }
    private void ChangeObjectsSequense(ActivatingObject[] objects)
    {
        ActivatingObject temp;
        int randIndex;
        for (int i = 0; i < objects.Length; i++)
        {
            randIndex = Random.Range(0, objects.Length);
            temp = objects[i];
            objects[i] = objects[randIndex];
            objects[randIndex] = temp;
        }
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].id = i;
            objects[i].image.transform.SetAsLastSibling();

        }
    }
    public void ResetAllActivatingObjects()
    {
        foreach (ActivatingObject obj in objectsToActivate)
        {
            obj.isActivated = false;
        }
        ActivationCount = 0;
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
