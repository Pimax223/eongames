﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ActivatingObject : MonoBehaviour {
    public ParticleSystem fireworkParticle;
    public ParticleSystem cloudParticle;
    public Image image;
    public int id;
    bool _isActivated=false;
    public bool isActivated {
        get {
            return _isActivated;
        }
        set {
            _isActivated = value;
            if (_isActivated)
            {
                fireworkParticle.Play(true);
                GameController.instance.ActivationCount++;
            }
            else
            {
                fireworkParticle.Stop(true);
                cloudParticle.Play(true);
            }
        }
    }

    public void InitializeObject()
    {
        image = GameObject.Find(gameObject.name + "Image").GetComponent<Image>();
    }
    public void TryToActivate()
    {
        if (GameController.instance.ActivationCount == id)
        {
            isActivated = true;
        }
        else
        {
            GameController.instance.ResetAllActivatingObjects();
        }
    }

}
